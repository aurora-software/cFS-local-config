/* Body file for function Function_2
 * Generated by TASTE on 2022-02-12 14:35:02
 * You can edit this file, it will not be overwritten
 * Provided interfaces : PI_1
 * Required interfaces : 
 * User-defined properties for this function:
 *   |_ Taste::Function_priority = 100
 *   |_ TASTE::Is_Component_Type = false
 *   |_ Taste::Needs_datastore = NO
 * Timers              : 
 */

#include "function_2.h"
//#include <stdio.h>


void function_2_startup(void)
{
   // Write your initialisation code, but DO NOT CALL REQUIRED INTERFACES
   // puts ("[Function_2] Startup");
}

void function_2_PI_PI_1(void)
{
   // Write your code here
}


